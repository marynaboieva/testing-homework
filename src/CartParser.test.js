import CartParser from './CartParser';
import path from 'path'
import fs from 'fs';

let parser;
let file=fs.readFileSync(path.resolve('./samples/cart.csv'), 'utf-8', 'r');//normal file
let file1 =fs.readFileSync(path.resolve('./src/cart1.csv'),'utf-8', 'r');//with an error in column name 
let file2 =fs.readFileSync(path.resolve('./src/cart2.csv'),'utf-8', 'r');//with a wrong cells.length
let file3 =fs.readFileSync(path.resolve('./src/cart3.csv'),'utf-8', 'r');//with an empty text cell
let file4 =fs.readFileSync(path.resolve('./src/cart4.csv'),'utf-8', 'r');//with a negative price 
let file6 =fs.readFileSync(path.resolve('./src/cart6.csv'),'utf-8', 'r');//with a symbol instead of number 
//cart5.csv -shorter file
beforeEach(() => {
	parser = new CartParser();
});

describe('validate', () => {
	it('should return eror when column name is wrong',()=>{
		let errors=parser.validate(file1);
		expect(errors).toMatchObject([{type:'header', row:0, column:0}])
	});
	it('should return error when table has less cells',()=>{
		let errors=parser.validate(file2);
		expect(errors).toMatchObject([{type:'row', row:1, column:-1},
		{ type: 'row',row: 2,column: -1}])
	})
	it('should return error when text field is empty',()=>{
		let errors=parser.validate(file3);
		expect(errors).toMatchObject([{type:'cell', row:1, column:0}])
	})
	it('should return error when quantity or price is negative number',()=>{
		let errors=parser.validate(file4);
		expect(errors).toMatchObject([{type:'cell', row:1, column:2}])
	})
	it('should return error when quantity or price is a symbol',()=>{
		let errors=parser.validate(file6);
		expect(errors).toMatchObject([{type:'cell', row:1, column:1}])
	})
});
describe('parse',()=>{
	it('should return json object if file is valid',()=>{
		let result =parser.parse(path.resolve('./src/cart5.csv'));
		expect(result).toMatchObject({"items": [{"name": "Mollis consequat", "price": 9, "quantity": 2}], "total": 18})
	})
	it('should throw error when file has error',()=>{
		function fn(){
			parser.parse(path.resolve('./src/cart2.csv'))
		}
		expect(fn).toThrowError('Validation failed!');
	})
})
describe('parseLine',()=>{
	it('should return object with key from column keys and values from CSV',()=>{
		let line='Mollis consequat,9.00,2';
		expect(parser.parseLine(line)).toMatchObject({ "name": "Mollis consequat","price": 9,"quantity": 2});
	})
})
describe('CartParser - integration test', () => {
	// Add your integration test here.
});